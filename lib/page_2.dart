import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  final numList = Iterable<int>.generate(5).toList();

  final title;

  SecondPage({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$title'),
      ),
      body: Center(
        child: RaisedButton(
            elevation: 10,
            child: Text('Back To HomeScreen'),
            color: Theme
                .of(context)
                .primaryColor,
            textColor: Colors.white,
            onPressed: () => Navigator.pop(context)),
      ),
      drawer: Drawer(
        child: ListView.separated(
            padding: EdgeInsets.only(left: 15, right: 15, top: 50),
            separatorBuilder: (BuildContext context, int index) =>
                Divider(
                  height: 5,
                ),
            itemCount: numList.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                color: Colors.green[500],
                child: Center(
                  child: Text(
                    '${numList[index]}',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                ),
              );
            }),

      ),
    );
  }
}