import 'package:flutter/material.dart';
import 'page_2.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Denis Test App 1',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: MyHomePage(title: 'Denis Test App 1'),
        routes: <String, WidgetBuilder>{
          '/a': (BuildContext context) => SecondPage(title: 'Page 2'),
        });
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _changePage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SecondPage(title: '${controller.text}')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          height: 150,
          width: 200,
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Enter next page title',
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _changePage,
        tooltip: 'Increment',
        child: Text(
          'PAGE\n   2',
          style: TextStyle(fontSize: 14),
        ),
      ),
    );
  }
}
